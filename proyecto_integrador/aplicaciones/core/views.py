from django.shortcuts import render
from django.views import View
# Create your views here.
from django.views.generic import TemplateView

""" Vistas basadas en clases, es una clase que actua como una funciona de la vista.
Como es una clasem se pueden crear diferentes instancias de la clase con diferentes argumentos,
para cambiar el comportamiento de la vista.
Esto tambien se conoce como vistas genericas, reutilizables o enchufables
"""
# class Inicio(View):
#     def get(self,request,*args,**kwargs):
#         return render(request,'base.html', {'titulo':'Inicio','url_anterior': '/' ,})

class Inicio(TemplateView):
    template_name='base.html'
    def get_context_data(self, **kwargs):
        context= super().get_context_data(**kwargs)
        context['titulo']="Pagina Principal"
        context['url_anterior']="/"
        return context

