from django.urls import path
from aplicaciones.core.views import Inicio

urlpatterns = [
    path('', Inicio.as_view(), name='Inicio'),
]
